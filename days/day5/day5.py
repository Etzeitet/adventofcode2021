from pathlib import Path
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input5.txt"

test_data = """0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2
"""

def get_segments(data):
    segments = [
        [
            int(i)
            for i in line.replace(" -> ", ",").strip().split(",")
        ]
        for line in data
    ]

    return segments


def walk_line(x1, y1, x2, y2):
    dx = x2 - x1
    dy = y2 - y1

    xsign = 1 if dx > 0 else -1
    ysign = 1 if dy > 0 else -1

    dx = abs(dx)
    dy = abs(dy)

    if dx > dy:
        xx, xy, yx, yy = xsign, 0, 0, ysign

    else:
        dx, dy = dy, dx
        xx, xy, yx, yy = 0, ysign, xsign, 0

    d = 2 * dy - dx
    y = 0

    for x in range(dx + 1):
        yield x1 + x * xx + y * yx, y1 + x * xy + y * yy

        if d >= 0:
            y += 1
            d -= 2 * dx

        d += 2 * dy


def part_1(data):
    grid = {}
    segments = get_segments(data)

    for segment in segments:

        if segment[0] == segment[2] or segment[1] == segment[3]:
            for p in walk_line(*segment):
                if p not in grid:
                    grid[p] = 1

                else:
                    grid[p] += 1

    return len([p for p in grid if grid[p] > 1])



def part_2(data):
    grid = {}
    segments = get_segments(data)

    for segment in segments:
            for p in walk_line(*segment):
                if p not in grid:
                    grid[p] = 1

                else:
                    grid[p] += 1

    return len([p for p in grid if grid[p] > 1])


def run():
    print(f"Day {DAY}")
    data = utils.load_input_as_lines(INPUT_PATH)

    print("Part 1 Test:", part_1(test_data.splitlines()))
    print("Part 1:", part_1(data))

    print("Part 2 Test:", part_2(test_data.splitlines()))
    print("Part 2:", part_2(data))
