from pathlib import Path
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input4.txt"

test_data = """7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7
"""


def get_number_draws(lines):
    return [int(num) for num in lines[0].split(",")]


def get_boards(lines):
    boards = []
    for i in range(2, len(lines), 6):
        board = [[int(num) for num in line.split()] for line in lines[i:i+5]]

        boards.append(board)

    return boards


def row_complete(draws, board):
    for row in board:
        if all(True if x in draws else False for x in row):
            return True

    return False


def column_complete(draws, board):
    inverted_board = [cols for cols in zip(*board)]
    return row_complete(draws, inverted_board)



def part_1(draws, boards):

    drawn = []
    for draw in draws:
        drawn.append(draw)

        for board in boards:
            if row_complete(drawn, board) or column_complete(drawn, board):
                unmarked_sum = sum(num for row in board for num in row if num not in drawn)
                return unmarked_sum, draw, unmarked_sum * draw



def part_2(draws, boards):
    drawn = []
    winners = []

    for draw in draws:
        drawn.append(draw)

        for board in boards:
            if board in winners:
                continue

            if row_complete(drawn, board) or column_complete(drawn, board):
                winners.append(board)

        if len(boards) == len(winners):
            break

    unmarked_sum = sum(num for row in winners[-1] for num in row if num not in drawn)

    return unmarked_sum, draw, unmarked_sum * draw


def run():
    print(f"Day {DAY}")
    data = utils.load_input_as_lines(INPUT_PATH)

    test_draws = get_number_draws(test_data.splitlines())
    test_boards = get_boards(test_data.splitlines())

    draws = get_number_draws(data)
    boards = get_boards(data)

    print("Part 1 Test:", part_1(test_draws, test_boards))
    print("Part 1", part_1(draws, boards))

    print("Part 2 Test:", part_2(test_draws, test_boards))
    print("Part 2", part_2(draws, boards))
