from pathlib import Path
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input13.txt"

test_data1 = """0,2
2,2

fold along y=1
""".splitlines()

test_data2 = """6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5
""".splitlines()


def get_page(lines: list[str]):

    line_break = lines.index("")
    dot_coords = lines[0:line_break]

    dots = []
    for dc in dot_coords:
        x, y = dc.split(",")
        dots.append((int(x), int(y)))

    max_x = max(dots, key=lambda x: x[0])[0]
    max_y = max(dots, key=lambda y: y[1])[1]

    page = []
    for y in range(max_y + 1):
        row = [None] * (max_x + 1)

        for dx, dy in dots:
            if dy == y:
                row[dx] = True

        page.append(row)

    return page


def get_folds(lines):
    line_break = lines.index("")
    instructions = lines[line_break + 1:]
    folds = []

    for f in instructions:
        axis, value = f.split()[-1].split("=")
        folds.append((axis, int(value)))

    return folds


def fold(page, x_fold=0, y_fold=0):
    # since we don't care about rows above x_fold or columns before y_fold
    # we only iterate after the fold lines
    for y in range(y_fold, len(page)):
        for x in range(x_fold, len(page[y])):

            # do nothing if no dot
            if not page[y][x]:
                continue

            # set new co-ordinates to current
            new_x = x
            new_y = y

            if x_fold:
                dx = x  - x_fold
                new_x = x_fold - dx

            if y_fold:
                dy = y  - y_fold
                new_y = y_fold - dy

            page[new_y][new_x] = True

        # chop off elements after fold line
        if x_fold:
            page[y] = page[y][:x_fold]

    # chop off rows after fold line
    if y_fold:
        page = page[:y_fold]

    return page

def print_page(page):
    for row in page:
        c = 0
        for i, col in enumerate(row):
            if i > 0 and i % 5 == 0:
                print("  ", end="")

            v = "\033[31m#\033[0m" if col else " "
            print(v, end="")



        print()



def part_1(data):
    page = get_page(data)
    folds = get_folds(data)

    print_page(page)

    for axis, value in folds[:1]:
        if axis == "x":
            fold_axis = {"x_fold": value}

        else:
            fold_axis = {"y_fold": value}

        page = fold(page, **fold_axis)

    print()
    print_page(page)

    return sum(row.count(True) for row in page)


def part_2(data):
    page = get_page(data)
    folds = get_folds(data)

    #print_page(page)

    for axis, value in folds:
        if axis == "x":
            fold_axis = {"x_fold": value}

        else:
            fold_axis = {"y_fold": value}

        page = fold(page, **fold_axis)

    print()
    print_page(page)



def run():
    print(f"Day {DAY}")
    data = utils.load_input_as_lines(INPUT_PATH)



    # print("Part 1 Test 1:", part_1(test_data1))
    # print("Part 1 Test 2:", part_1(test_data2))
    # print("Part 1:", part_1(data))
    # print("Part 2 Test:", part_2(test_data2))
    print("Part 2:", part_2(data))
