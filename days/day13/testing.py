import random

paper = [
    "####   #   #   ####   ##### #####",
    "#   #  #   #   #   #    #   #    ",
    "####    # #    #   #    #   #####",
    "#        #     #   #    #       #",
    "#        #     ####   ##### #####",
]


paper = [list(row) for row in paper]

unfolded = [
    [None] * (len(paper[0]) + 1) * 2
    for _ in range( (len(paper) * 2) + 1 )
]

for y, row in enumerate(paper):
    for x, col in enumerate(row):
        unfolded[y][x] = paper[y][x]

y_fold = 5
for y, row in enumerate(unfolded[:y_fold]):
    for x, col in enumerate(row):
        keep = random.random() > 0.5
        flip = random.random() > 0.5

        if flip:
            new_y = y_fold + (y_fold - y)
            unfolded[new_y][x] = True

            if not keep:
                unfolded[y][x] = None

x_fold = 34
for y, row in enumerate(unfolded):
    for x, col in enumerate(row[:x_fold]):
        keep = random.random() > 0.5
        flip = random.random() > 0.5

        if flip:
            new_x = x_fold + (x_fold - x)
            unfolded[y][new_x] = True

            if not keep:
                unfolded[y][x] = None

for y, row in enumerate(unfolded):
    for x, col in enumerate(row):
        if col:
            print(f"{x},{y}")
