from collections import defaultdict
from io import open_code
from pathlib import Path
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input22.txt"

class Reactor:
    def __init__(self, area, restrict=True):
        self._area = area
        self._reactor = defaultdict(lambda: False)
        self.restrict = restrict

    def get_cuboid(self, coords, restrict=50):
        cuboid = []

        if self.restrict:
            if not all(-50 <= v <= 50 for v in coords):
                return cuboid

        for x in range(coords[0], coords[1] + 1):
            for y in range(coords[2], coords[3] + 1):
                for z in range(coords[4], coords[5] + 1):
                    cuboid.append((x, y, z))

        return cuboid

    def update(self, on, coords):
        cuboid = self.get_cuboid(coords)

        for cube in cuboid:
            self._reactor[cube] = on

    @property
    def on_count(self):
        count = 0
        for coords, on in self._reactor.items():
            if self.restrict:
                if all(-50 <= v <= 50 for v in coords) and on:
                    count += 1

            elif on:
                count += 1

        return count

    def reboot(self, procedure):
        for instruction in procedure:
            self.update(instruction[0], instruction[1:])


def get_procedure(lines):
    procedure = []

    for line in lines:
        on, cuboid = line.split()
        x, y, z = cuboid.split(",")

        on = on == "on"
        x1, x2 = map(int, x[2:].split(".."))
        y1, y2 = map(int, y[2:].split(".."))
        z1, z2 = map(int, z[2:].split(".."))

        procedure.append((on, x1, x2, y1, y2, z1, z2))

    return procedure


def part_1(data):
    procedure = get_procedure(data)

    reactor = Reactor(50)
    reactor.reboot(procedure)

    print(reactor.on_count)


def part_2(data):
    procedure = get_procedure(data)

    reactor = Reactor(50, False)
    reactor.reboot(procedure)

    print(reactor.on_count)


def run():
    print(f"Day {DAY}")
    test_data1 = """on x=10..12,y=10..12,z=10..12
on x=11..13,y=11..13,z=11..13
off x=9..11,y=9..11,z=9..11
on x=10..10,y=10..10,z=10..10
""".splitlines()

    test_data2 = """on x=-20..26,y=-36..17,z=-47..7
on x=-20..33,y=-21..23,z=-26..28
on x=-22..28,y=-29..23,z=-38..16
on x=-46..7,y=-6..46,z=-50..-1
on x=-49..1,y=-3..46,z=-24..28
on x=2..47,y=-22..22,z=-23..27
on x=-27..23,y=-28..26,z=-21..29
on x=-39..5,y=-6..47,z=-3..44
on x=-30..21,y=-8..43,z=-13..34
on x=-22..26,y=-27..20,z=-29..19
off x=-48..-32,y=26..41,z=-47..-37
on x=-12..35,y=6..50,z=-50..-2
off x=-48..-32,y=-32..-16,z=-15..-5
on x=-18..26,y=-33..15,z=-7..46
off x=-40..-22,y=-38..-28,z=23..41
on x=-16..35,y=-41..10,z=-47..6
off x=-32..-23,y=11..30,z=-14..3
on x=-49..-5,y=-3..45,z=-29..18
off x=18..30,y=-20..-8,z=-3..13
on x=-41..9,y=-7..43,z=-33..15
on x=-54112..-39298,y=-85059..-49293,z=-27449..7877
on x=967..23432,y=45373..81175,z=27513..53682
""".splitlines()

    data = utils.load_input_as_lines(INPUT_PATH)

    part_1(test_data1)
    part_1(test_data2)
    part_1(data)

    part_2(test_data1)
    part_2(test_data2)
