import statistics
from pathlib import Path
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input10.txt"

test_data = """[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]
""".splitlines()

pairs = {
    "(": ")",
    "[": "]",
    "{": "}",
    "<": ">"
}

scores = {
    ")": 3,
    "]": 57,
    "}": 1197,
    ">": 25137
}

part2_scores = {
    ")": 1,
    "]": 2,
    "}": 3,
    ">": 4
}


def parse_corrupted(line):
    stack = []

    for i, char in enumerate(line):
        if char in pairs:
            # counter += 1
            stack.append(i)

        if char in pairs.values() and stack:
            # counter -= 1
            start = stack.pop()

            r = line[start: i + 1]

            if r[-1] != pairs[r[0]]:
                return (start, i, r[-1])


def repair_line(line):
    stack = []

    for char in line:
        if char in pairs:
            stack.append(char)

        elif char in pairs.values() and stack:
            stack.pop()

    return stack[::-1]

# [
#     (
#         {
#             (
#                 <
#                     (
#                         ()
#                     )
#                     []
#                 >
#                 [
#                     [
#                         {
#                             []
#                             {
#                                 <
#                                     ()
#                                     <>
#                                 >

def part_1(data):
    score = 0
    for line in data:
        result = parse_corrupted(line)
        if result:

            score += scores[result[-1]]

    return score


def part_2(data):
    incomplete = [line for line in data if not parse_corrupted(line)]

    scores = []

    for line in incomplete:
        missing = repair_line(line)

        score = 0
        for m in missing:
            score *= 5
            score += part2_scores[pairs[m]]

        scores.append(score)

    return statistics.median(scores)


def run():
    print(f"Day {DAY}")
    data = utils.load_input_as_lines(INPUT_PATH)

    print("Part 1 Test:", part_1(test_data))
    print("Part 1:", part_1(data))

    print("Part 2 Test:", part_2(test_data))
    print("Part 2:", part_2(data))
