from pathlib import Path
from random import randrange
from functools import cache, lru_cache
import itertools
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input21.txt"


class PlayerWins(Exception):
    pass


class DDice:
    """
    Deterministic Dice
    """

    def __init__(self, rolls=100):
        self.max_rolls = 100
        self.last_roll = 0
        self.total_rolls = 0

    def roll(self, count=1):
        for _ in range(count):
            self.last_roll = self.last_roll % self.max_rolls
            self.last_roll += 1
            self.total_rolls += 1

            yield self.last_roll


class Player:
    def __init__(self, name: str):
        self.name = name
        self.score = 0

    def add_to_score(self, position: int):
        self.score += position

    def __hash__(self):
        return hash(self.name)


class Board:
    def __init__(self, random: bool = True):
        self.positions = 10
        self.players = {}
        self._random = random
        self._scores = list(range(1, 11))

    def add_player(self, player: Player, position: int = None):
        if self._random:
            position = randrange(0, self.positions)

        self.players[player] = (position - 1) % self.positions

    def move_player(self, player: Player, moves: int):
        position = self.players[player]
        new_position = (position + moves) % self.positions

        self.players[player] = new_position
        player.add_to_score(self._scores[new_position])

        if player.score >= 1000:
            raise PlayerWins()


def part_1():
    pass


@cache
def part2(a, b, score_a=0, score_b=0):
    if score_b >= 21:
        return (0, 1)

    wa, wb = 0, 0
    weights = (1, 3, 6, 7, 6, 3, 1)
    for i in range(7):
        position_a = (a + i + 3) % 10
        wins_for_b, wins_for_a = part2(b, position_a, score_b, score_a + position_a + 1)
        wa, wb = wa + wins_for_a * weights[i], wb + wins_for_b * weights[i]
    return (wa, wb)


@cache
def part2a(a, b, sa=0, sb=0):
    if sb >= 21:
        return (0, 1)
    wa, wb = 0, 0
    weights = (1, 3, 6, 7, 6, 3, 1)
    for i in range(7):
        ta = (a + i + 3) % 10
        rb, ra = part2a(b, ta, sb, sa + ta + 1)
        wa, wb = wa + ra * weights[i], wb + rb * weights[i]
    return (wa, wb)


@lru_cache(maxsize=None)
def part2b(a_posit, o_posit, a_score=0, o_score=0):
    A, O = 0, 0
    for roll in itertools.product(range(1, 4), repeat=3):
        posit = ((a_posit + sum(roll)) - 1) % 10 + 1
        score = a_score + posit
        o, a = (0, 1) if score >= 21 else part2b(o_posit, posit, o_score, score)
        A, O = A + a, O + o
    return A, O


def run():
    print(f"Day {DAY}")
    data = utils.load_input_as_lines(INPUT_PATH)

    ddice = DDice(10)

    player1 = Player("player1")
    player2 = Player("player2")

    board = Board(False)
    board.add_player(player1, 4)  # int(data[0].split()[-1].strip()))
    board.add_player(player2, 8)  # int(data[1].split()[-1].strip()))

    exit = False
    while not exit:
        for player in board.players:
            p_roll = sum(ddice.roll(3))

            try:
                board.move_player(player, p_roll)

            except PlayerWins:
                print("Player 1:", player1.score)
                print("Player 2:", player2.score)
                print(ddice.total_rolls)

                loser = player1 if player1.score < player2.score else player2
                print(loser.score * ddice.total_rolls)
                exit = True
                break

    start_a = int(data[0].split()[-1].strip())
    start_b = int(data[1].split()[-1].strip())
    print(part2(4, 8))
    print(part2a(4, 8))
    print(part2b(start_a, start_b))
