from pathlib import Path
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input6.txt"


def part_1(fish, days):
    for _ in range(days):
        for i in range(len(fish)):
            age = fish[i] - 1

            if age < 0:
                fish.append(8)
                age = 6

            fish[i] = age

    return len(fish)


def part_2(fish, days):
    """
    Rather than hold onto a list of actual fish, track the population by age.
    For each day, any fish at 0 days will be removed from the population for 0, then
    added to the population of age 6 and age 8 (new fish per spawn).
    """
    population = [0] * 9

    # build population
    for age in fish:
        population[age] += 1

    for day in range(days):
        # population count at 0 days is removed
        zeroday = population.pop(0)

        # 8 day population is reset
        population.append(0)

        # add population of spawning fish to age 6 population
        population[6] += zeroday

        # add spawned fish to age 8 population
        population[8] += zeroday

    return sum(population)



def run():
    print(f"Day {DAY}")
    data = utils.load_input_as_lines(INPUT_PATH)
    data = [int(age) for fish in data for age in fish.split(",")]
    test_data = [3, 4, 3, 1, 2]

    print("Part 1 Test:", part_2(test_data, 80))
    print("Part 1:", part_2(data, 80))

    print("Part 2 Test:", part_2(test_data, 256))
    print("Part 2:", part_2(data, 256))
