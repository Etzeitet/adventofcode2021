from pathlib import Path
from copy import deepcopy
import functools
import timeit
import days.util.utils as utils

Image = list[list[int]]
Algorithm = str
PixelBlock = list[int]

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input20.txt"

test_data = """#.#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#...

#..#.
#....
##..#
..#..
..###
""".splitlines()


class Image:
    def __init__(self, image: Image, algorithm: Algorithm):
        self.image = image
        self.algorithm = algorithm

        self.lit = 0

    def enhance(self, default: int):
        height = len(self.image)
        width = len(self.image[0])

        padded_image = [
            [default for x in range(width + 2)]
            for y in range(height + 2)
        ]

        for y in range(height):
            for x in range(width):
                padded_image[y+1][x+1] = self.image[y][x]

        #print_image(padded_image)

        new_image = deepcopy(padded_image)

        self.lit = 0
        for y, row in enumerate(new_image):
            for x, p in enumerate(row):

                block = get_neighbours(x, y, padded_image, default)
                new_value = self.block_to_value(block)
                new_image[y][x] = new_value

                if new_value:
                    self.lit += 1

        self.image = new_image

    def get_neighbours(self, x: int, y: int, default: int = 0) -> PixelBlock:
        neighbours = []
        min_x = 0
        max_x = len(self.image[0]) - 1
        min_y = 0
        max_y = len(self.image) - 1

        for dx, dy in [(-1, -1), (0, -1), (1, -1), (-1, 0), (0, 0), (1, 0), (-1, 1), (0, 1), (1, 1)]:
            nx = x + dx
            ny = y + dy

            if min_x <= nx <= max_x and min_y <= ny <= max_y:
                neighbours.append(self.image[ny][nx])

            else:
                neighbours.append(default)

        return tuple(neighbours)

    @functools.lru_cache(maxsize=None)
    def block_to_value(self, block: PixelBlock) -> int:
        index = 0

        shift = 8
        for value in block:
            index |= value << shift
            shift -= 1

        return 1 if self.algorithm[index] == "#" else 0



def get_input(lines: list[str]) -> Image:
    image = [[1 if p == "#" else 0 for p in line] for line in lines[2:]]
    return image


def get_enhancement_algorithm(lines: list[str]) -> Algorithm:
    return lines[0]

def get_neighbours(x: int, y: int, image: Image, default: int = 0) -> PixelBlock:
    neighbours = []
    min_x = 0
    max_x = len(image[0]) - 1
    min_y = 0
    max_y = len(image) - 1

    for dx, dy in [(-1, -1), (0, -1), (1, -1), (-1, 0), (0, 0), (1, 0), (-1, 1), (0, 1), (1, 1)]:
        nx = x + dx
        ny = y + dy

        if min_x <= nx <= max_x and min_y <= ny <= max_y:
            neighbours.append(image[ny][nx])

        else:
            neighbours.append(default)

    return tuple(neighbours)


@functools.lru_cache(maxsize=None)
def block_to_value(block: PixelBlock, algorithm: Algorithm) -> int:
    index = 0

    shift = 8
    for value in block:
        index |= value << shift
        shift -= 1

    return 1 if algorithm[index] == "#" else 0

@functools.lru_cache(maxsize=None)
def block_to_value2(block: PixelBlock, algorithm: Algorithm) -> int:
    #block = [str(x) for x in block]
    index = int("".join(map(str, block)), 2)

    return 1 if algorithm[index] == "#" else 0


def enhance(image: Image, algorithm: Algorithm, default: int):
    height = len(image)
    width = len(image[0])

    padded_image = [
        [default for x in range(width + 2)]
        for y in range(height + 2)
    ]

    for y in range(height):
        for x in range(width):
            padded_image[y+1][x+1] = image[y][x]

    #print_image(padded_image)

    new_image = deepcopy(padded_image)

    for y, row in enumerate(new_image):
        for x, p in enumerate(row):

            block = get_neighbours(x, y, padded_image, default)
            new_value = block_to_value(block, algorithm)
            new_image[y][x] = new_value

    # new_image = new_image[1:-1]
    # for row in new_image:
    #     row.pop(0)
    #     row.pop()

    return new_image


def count_lit(image: Image) -> int:

    return sum(
        1
        for row in image
        for pixel in row
        if pixel == 1
    )


def print_image(image: Image) -> None:
    for row in image:
        for pixel in row:
            if pixel == 1:
                print("\033[31m#\033[0m", end="")

            elif pixel == 0:
                print(".", end="")

            else:
                print(pixel, end="")

        print()


def part_1(image: Image, algorithm: Algorithm) -> None:
    image = enhance(image, algorithm, 0)
    print_image(image)
    print()

    image = enhance(image, algorithm, 1)
    print_image(image)
    print()

    return count_lit(image)


def part_2(image: Image, algorithm: Algorithm):
    for i in range(50):
        if i % 2 == 0:
            default = 0

        else:
            default = 1

        image = enhance(image, algorithm, default)

    return count_lit(image)


def bench_run(part, test=False):
    data = utils.load_input_as_lines(INPUT_PATH)

    if test:
        img = get_input(test_data)
        algo = get_enhancement_algorithm(test_data)

    else:
        img = get_input(data)
        algo = get_enhancement_algorithm(data)


    image = Image(img, algo)

    if part == 1:
        for i in range(2):
            if i % 2 == 0:
                image.enhance(0)

            else:
                image.enhance(1)

        print(image.lit)

    elif part == 2:
        for i in range(50):
            if i % 2 == 0:
                image.enhance(0)

            else:
                image.enhance(1)

        print(image.lit)


def run():
    print(f"Day {DAY}")
    data = utils.load_input_as_lines(INPUT_PATH)

    image = get_input(test_data)
    algo = get_enhancement_algorithm(test_data)
    part_2(image, algo)
    # print("Part 1 Test:", part_2(image, algo))

    # image = get_input(data)
    # algo = get_enhancement_algorithm(data)
    #print("Part 2:", part_2(image, algo))
