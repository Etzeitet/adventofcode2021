from dataclasses import dataclass
from pathlib import Path
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input25.txt"


@dataclass
class Point:
    previous: str = None
    current: str = "."

    @property
    def moved(self):
        return self.previous != self.current

    def available(self, point: "Point"):
        # if this point is empty and wasn't previously a matching type
        # e.g.
        # if a point was moved from >>. to >.>, the second point would be marked
        # as previously being >, this will prevent a matching point being moved
        # into the now empty space
        return self.current == "." and self.previous != point.current

    def reset(self):
        self.previous = self.current

    def __post_init__(self):
        self.previous = self.current

    def __str__(self):
        return self.current

    def __repr__(self):
        return f"{self.previous if self.previous else '-'}/{self.current}"


@dataclass
class SeaFloor:
    data: list[list[str]]
    board: list[list[Point]] = None
    height: int = 0
    width: int = 0
    moves: int = 0
    finished: bool = False

    def __post_init__(self):
        self.board = []

        for line in self.data:
            row = [
                Point(current=x)
                for x in line
            ]

            self.board.append(row)

        self.height = len(self.board)
        self.width = len(row)

    def reset_points(self):
        for row in self.board:
            for point in row:
                point.reset()

    def step_east(self):
        moves = 0
        for y in range(self.height):
            for x in range(self.width):
                new_x = (x + 1) % self.width

                point = self.board[y][x]
                if point.moved:
                    continue

                new_point = self.board[y][new_x]

                if point.current == ">":
                    if new_point.available(point):
                        point.previous = ">"
                        point.current = "."
                        new_point.previous = "."
                        new_point.current = ">"

                        moves += 1
        return moves

    def step_south(self):
        moves = 0

        for y in range(self.height):
            for x in range(self.width):
                new_y = (y + 1) % self.height

                point = self.board[y][x]
                if point.moved:
                    continue

                new_point = self.board[new_y][x]

                if point.current == "v":
                    if new_point.available(point):
                        point.previous = "v"
                        point.current = "."
                        new_point.previous = "."
                        new_point.current = "v"

                        moves += 1

        return moves

    def step(self):
        moves = 0
        moves += self.step_east()
        moves += self.step_south()
        self.reset_points()

        if moves == 0:
            self.finished = True

    def print(self, n=None):
        for y, row in enumerate(self.board):
            if n is not None and y != n:
                continue

            print("".join(str(p) for p in row))


def part_1(seafloor: SeaFloor, max_steps=None):

    steps = 0
    while True:
        seafloor.step()
        seafloor.print()
        print()

        steps += 1

        if seafloor.finished or (max_steps and steps >= max_steps):
            break

    return steps


def part_2():
    pass


def run():
    print(f"Day {DAY}")
    data = utils.load_input_as_lines(INPUT_PATH)

    test_data = [
        "v...>>.vv>",
        ".vv>>.vv..",
        ">>.>v>...v",
        ">>v>>.>.v.",
        "v>v.vv.v..",
        ">.>>..v...",
        ".vv..>.>v.",
        "v.v..>>v.v",
        "....v..v.>",
    ]

    seafloor_test = SeaFloor(test_data)
    seafloor = SeaFloor(data)

    print("Part 1 Test:", part_1(seafloor_test))
    print("Part 1:", part_1(seafloor))
