from pathlib import Path
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input3.txt"


def part_1(data):
    bit_count = len(data[0])
    mask = int("1" * bit_count, 2)
    mid = len(data) / 2

    gamma = "".join("1" if col.count("1") >= mid else "0" for col in zip(*data))
    gamma = int(gamma, 2)
    epsilon = gamma ^ mask

    return gamma, epsilon, gamma * epsilon



def part_2_helper(data, type):
    col = 0
    stack = data.copy()

    if type == "oxygen":
        i = 0

    else:
        i = 1

    while len(stack) > 1:
        # get either gamma (i = 0) or epsilon (i = 1) rating
        x = part_1(stack)[i]
        x = f"{bin(x)[2:]:0>{len(data[0])}}"

        stack = [num for num in stack if num[col] == x[col]]

        col += 1

    return stack

def part_2(data):
    gamma_stack = part_2_helper(data, "oxygen")
    epsilon_stack = part_2_helper(data, "co2")

    oxygen = int(gamma_stack[0], 2)
    co2 = int(epsilon_stack[0], 2)

    return oxygen * co2





def run():
    print(f"Day {DAY}")
    data = utils.load_input_as_lines(INPUT_PATH)

    test_data = [
        "00100",
        "11110",
        "10110",
        "10111",
        "10101",
        "01111",
        "00111",
        "11100",
        "10000",
        "11001",
        "00010",
        "01010",
    ]

    print("Part 1 Test:", part_1(test_data)[-1])
    print("Part 1:", part_1(data)[-1])

    print("Part 2 Test:", part_2(test_data))
    print("Part 2:", part_2(data))
