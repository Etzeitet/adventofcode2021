from pathlib import Path
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / ":INPUT_PATH:"


def part_1():
    pass


def part_2():
    pass


def run():
    print(f"Day {DAY}")
    data = utils.load_input_as_lines(INPUT_PATH)
