from pathlib import Path
from typing import Any, List


def load_input_as_string(path: Path) -> str:
    with path.open("r") as f:
        data = f.read()

    return data


def load_one_line_as_list(path: Path, delim: str = ",", as_int: bool = False, as_float: bool = False) -> List[int|float|str]:
    line = load_input_as_string(path).split(delim)

    if as_int:
        func = int

    if as_float:
        func = float

    return [func(x) for x in line]


def load_input_as_lines(path: Path, as_int: bool = False, as_float: bool = False) -> List[int|float|str]:
    with path.open("r") as f:
        data = f.read().splitlines()

    if as_int:
        func = int

    elif as_float:
        func = float

    else:
        func = str


    return [func(x) for x in data]
