from math import prod
import operator
from pathlib import Path
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input16.txt"


class Packet:
    version_total = 0

    def __init__(self, s):
        self.s = s
        self.ops = [
        sum,
        prod,
        min,
        max,
        None,
        operator.gt,
        operator.lt,
        operator.eq
    ]

    def read(self, n, as_int=True):
        bits = self.s[:n]

        if as_int:
            bits = int(bits, 2)

        self.s = self.s[n:]
        return bits

    def get_value(self):
        value_bits = ""
        s = 1
        while s != 0:
            s, v = self.read(1), self.read(4, False)
            value_bits += v

        return int(value_bits, 2)

    def parse(self):
        version = self.read(3)

        # increment total for Part 1, this could be a global but seems neater to use
        # class variable
        Packet.version_total += version

        type_ = self.read(3)

        # type 4 is a literal value, so get it and return
        if type_ == 4:
            return self.get_value()

        len_type = self.read(1)
        values = []

        # len_type 0 uses next 15 bits to represent how many bits sub-packets make up
        if len_type == 0:
            l = self.read(15)

            # create a new Packet to manage these sub-packets (treat the same as the outer packet)
            sub_packet = Packet(self.read(l, False))
            while sub_packet.s:
                v = sub_packet.parse()
                values.append(v)

        else:
            # len_type 1 uses next 11 its to represent how many packets make up sub packet
            c = self.read(11)

            # since we know how any packets needs to be read just go down another recursion
            # step for each one.
            for _ in range(c):
                v = self.parse()
                values.append(v)

        # types map to operations. All ops before type 5 (ignoring 4) take an iterable
        if type_ < 5:
            return self.ops[type_](values)

        # other take just two values, so unpack values
        else:
            return self.ops[type_](*values)

# def read(packet, n, as_int=False):
#     bits = packet[:n]
#     if as_int:
#         bits = int(bits, 2)

#     return bits, packet[n:]


# def get_value(packet):
#     value_bits = ""
#     for i in range(0, len(packet), 5):
#         start = packet[i]
#         value_bits += packet[i+1:i+5]

#         if start == "0":
#             break

#     return int(value_bits, 2), packet[i+5:]


# def calculate(type, *values):
#     ops = [
#         sum,
#         prod,
#         min,
#         max,
#         None,
#         operator.gt,
#         operator.lt,
#         operator.eq
#     ]

#     values_ = [v["value"] for v in values]
#     if type in range(5):
#         return int(ops[type](values_))

#     else:
#         return int(ops[type](*values_))


# def parse_packet(packet, packet_list=None):
#     if packet_list is None:
#         packet_list = []

#     packet_ = {}
#     packet_["version"], packet = read(packet, 3, True)
#     packet_["type"], packet = read(packet, 3, True)
#     packet_list.append(packet_["version"])

#     if packet_["type"] == 4:
#         packet_["value"], packet = get_value(packet)
#         return packet_, packet, packet_list

#     else:
#         packet_["length_id"], packet = read(packet, 1, True)

#         if packet_["length_id"] == 0:
#             subpackets_len, packet = read(packet, 15, True)

#             packets = []
#             subpackets, packet = read(packet, subpackets_len)
#             while subpackets:
#                 sub_packet, subpackets, _ = parse_packet(subpackets, packet_list)
#                 packets.append(sub_packet)

#                 if sub_packet["type"] != 4:
#                     sub_packet["value"] = calculate(sub_packet["type"], *sub_packet["packets"])

#         else:
#             subpacket_count, packet = read(packet, 11, True)
#             packets = []
#             for _ in range(subpacket_count):
#                 sub_packet, packet, _ = parse_packet(packet, packet_list)
#                 packets.append(sub_packet)

#         packet_["packets"] = packets
#         # packet_list.extend(packets)

#         return packet_, packet, packet_list


def part_1(packet_hex):
    dec = int(packet_hex, 16)
    packet_ = bin(dec)[2:].zfill(len(packet_hex) * 4)

    # packet, _, packet_list = parse_packet(packet_)

    Packet.version_total = 0

    packet = Packet(packet_)
    v = packet.parse()

    return packet.version_total

def part_2(packet_hex):
    dec = int(packet_hex, 16)
    packet_ = bin(dec)[2:].zfill(len(packet_hex) * 4)

    # packet, _, packet_list = parse_packet(packet_)

    Packet.version_total = 0

    packet = Packet(packet_)
    v = packet.parse()
    return v


def run():
    print(f"Day {DAY}")
    data = utils.load_input_as_string(INPUT_PATH).strip()

    test_packets_p1 = [
        ("8A004A801A8002F478", 16),
        ("620080001611562C8802118E34", 12),
        ("C0015000016115A2E0802F182340", 23),
        ("A0016C880162017C3686B18A3D4780", 31)
    ]

    test_packets_p2 = [
        ("C200B40A82", 3),
        ("04005AC33890", 54),
        ("880086C3E88112", 7),
        ("9C0141080250320F1802104A08", 1)
    ]

    for packet_hex, expected in test_packets_p1:
        version_sum = part_1(packet_hex=packet_hex)
        print(f"Part 1 Test ({packet_hex}): ", version_sum == expected)

    print("Part 1:", part_1(data))
    print()

    for packet_hex, expected in test_packets_p2:
        value = part_2(packet_hex=packet_hex)
        print(f"Part 2 Test ({packet_hex}): ", value == expected)

    print("Part 2:", part_2(data))
