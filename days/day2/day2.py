from pathlib import Path
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input2.txt"


def part_1(data):
    position = [0, 0]

    for line in data:
        direction, distance = line.split()
        distance = int(distance)

        if direction == "forward":
            position[0] += distance

        elif direction == "up":
            position[1] -= distance

        elif direction == "down":
            position[1] += distance

    return position[0] * position[1]


def part_2(data):
    position = [0, 0, 0]

    for line in data:
        direction, distance = line.split()
        distance = int(distance)

        if direction == "forward":
            position[0] += distance
            position[1] += (distance * position[2])

        elif direction == "up":
            position[2] -= distance

        elif direction == "down":
            position[2] += distance

    return position[0] * position[1]


def run():
    print(f"Day {DAY}")
    data = utils.load_input_as_lines(INPUT_PATH)

    print("Part 1:", part_1(["forward 5", "down 5", "forward 8", "up 3", "down 8", "forward 2"]))
    print("Part 1:", part_1(data))

    print("Part 2:", part_2(["forward 5", "down 5", "forward 8", "up 3", "down 8", "forward 2"]))
    print("Part 2:", part_2(data))
