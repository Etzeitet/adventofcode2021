from collections import defaultdict
from pathlib import Path
from typing import Counter
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input14.txt"

test_data = """NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C""".splitlines()


def get_polymer(lines):
    polymer = list(lines[0])
    instructions = dict(i.split(" -> ") for i in lines[2:])

    return polymer, instructions


def build_polymer(polymer, instructions, steps):
    for _ in range(steps):
        new_polymer = polymer.copy()

        offset = 1
        for i in range(len(polymer)):
            pair = "".join(polymer[i:i+2])
            insert = instructions.get(pair)

            if insert:
                new_polymer.insert(i + offset, insert)
                offset += 1

        polymer = new_polymer

    return "".join(polymer)

def part_1(data, steps):
    polymer, instructions = get_polymer(data)
    new_polymer = build_polymer(polymer, instructions, steps)

    counter = Counter(new_polymer)
    mc = counter.most_common()
    return new_polymer, mc[0][1] - mc[-1][1]




def part_2(data, steps):
    polymer, instructions = get_polymer(data)

    ##chars = defaultdict(int, Counter(polymer))
    chars = Counter(polymer)
    pairs = defaultdict(int)

    for pair in zip(polymer, polymer[1:]):
        pair = "".join(pair)
        pairs[pair] += 1

    for _ in range(steps):
        new_pairs = defaultdict(int)

        for pair, insert in instructions.items():
            if pair in pairs:
                left = f"{pair[0]}{insert}"
                right = f"{insert}{pair[1]}"

                inserts = pairs[pair]
                new_pairs[left] += 1 * inserts
                new_pairs[right] += 1 * inserts
                #chars[insert] += 1 * inserts
                chars.update({insert: (1 * inserts)})

        pairs = new_pairs

    counter = Counter(chars)
    #mc = counter.most_common()
    mc = chars.most_common()
    return mc[0][1] - mc[-1][1]

def run():
    print(f"Day {DAY}")
    data = utils.load_input_as_lines(INPUT_PATH)

    # print("Part 1 Test:", part_1(test_data, 2)[1])
    # assert part_1(test_data, 2)[0] == "NBCCNBBBCBHCB"
    # assert part_1(test_data, 3)[0] == "NBBBCNCCNBBNBNBBCHBHHBCHB"
    # assert part_1(test_data, 4)[0] == "NBBNBNBBCCNBCNCCNBBNBBNBBBNBBNBBCBHCBHHNHCBBCBHCB"

    # print("Part 1:", part_1(data, 10)[1])

    # print("Part 2 Test:", part_2(test_data, 2))
    print("Part 2:", part_2(data, 10000))
