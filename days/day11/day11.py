import sys

from collections import deque
from dataclasses import dataclass
from pathlib import Path
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input11.txt"

test_data = """5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526
""".splitlines()


@dataclass
class Squid:
    x: int
    y: int
    power: int
    max_x: int = 0
    max_y: int = 0
    flashed: bool = False
    flashes: int = 0

    def flash(self) -> bool:
        """
        Flash squid if power level high enough.

        Return True if flashed, False otherwise.
        """

        if self.power > 9:
            self.flashed = True
            self.flashes += 1
            self.power = 0

            return True

        return False

    def increase_power(self):
        self.power += 1

        # if power has been increased, means we are done being flashed (woo!)
        self.flashed = False

    def get_neighbours(self, grid: list["Squid"]) -> list["Squid"]:
        """
        Get a list of up to 8 neighbours of this squid from grid.
        """
        n = []
        for dx, dy in (
            (-1, -1),
            (0, -1),
            (1, -1),
            (1, 0),
            (1, 1),
            (0, 1),
            (-1, 1),
            (-1, 0),
        ):
            nx = self.x + dx
            ny = self.y + dy

            if 0 <= nx <= self.max_x and 0 <= ny <= self.max_y:
                n.append(grid[ny][nx])

        return n

    def __repr__(self):
        return f"{self.power}"


def create_map(hmap: list[str]) -> list[list[Squid]]:
    """
    Create grid of integers from list of strings:

    ["1234", "5678", ...] => [[1, 2, 3, 4], [5, 6, 7, 8], ...]
    """
    max_x = len(hmap[0]) - 1
    max_y = len(hmap) - 1

    squids = []
    for y, row in enumerate(hmap):
        srow = [
            Squid(x=x, y=y, power=int(p), max_x=max_x, max_y=max_y)
            for x, p in enumerate(row)
        ]

        squids.append(srow)

    return squids


def flash(squids: list[list[Squid]], squid: Squid):
    queue = deque()
    queue.append(squid)

    while queue:
        current_squid: Squid = queue.popleft()

        if current_squid.flashed:
            continue

        if current_squid.flash():
            queue.extend(current_squid.get_neighbours(squids))

        else:
            current_squid.increase_power()
            if current_squid.power > 9:
                queue.append(current_squid)


def run_step(squids: list[list[Squid]]):
    for row in squids:
        for squid in row:
            squid.increase_power()

    for row in squids:
        for squid in row:
            if squid.power > 9:
                flash(squids, squid)

    return squids


def part_1(squids, steps):
    for _ in range(steps):
        run_step(squids)

    flashes = 0
    for row in squids:
        for squid in row:
            flashes += squid.flashes
            print(f"{squid.power:>2} ", end="")

        print()

    print("Flashes:", flashes)


def part_2(squids: list[list[Squid]]):
    steps = 0

    while True:
        run_step(squids)
        steps += 1

        flashed = []
        for row in squids:
            for squid in row:
                flashed.append(squid.flashed)

        if all(flashed):
            return steps


def run():
    print(f"Day {DAY}")
    data = utils.load_input_as_lines(INPUT_PATH)

    test1 = """11111
19991
19191
19991
11111
""".splitlines()

    squids = create_map(test_data)
    steps = int(sys.argv[2])

    part_1(create_map(test_data), steps)
    part_1(create_map(data), steps)

    print("Part 2 Test:", part_2(create_map(test_data)))
    print("Part 2:", part_2(create_map(data)))
