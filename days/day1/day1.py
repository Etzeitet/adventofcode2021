from pathlib import Path
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input1.txt"


def part_1(data):
    increased_count = 0
    previous = data[0]
    for depth in data[1:]:
        if depth > previous:
            increased_count += 1

        previous = depth

    return increased_count


def part_1a(data):
    increased_count = sum(1 for i, x in enumerate(data[1:], 1) if x > data[i-1])
    return increased_count


def part_2(data):
    increased_count = 0
    previous = sum(data[:3])
    i = 0

    while i < len(data):
        window = data[i : i+3]

        window_sum = sum(window)
        if window_sum > previous and len(window) == 3:
            increased_count += 1

        previous = window_sum
        i += 1

    return increased_count


def part_2a(data):
    increased_count = 0
    previous = data[0]

    for i in range(3, len(data)):

        if data[i] > data[i - 3]:
            increased_count += 1

    return increased_count


def part_2b(data):
    increased_count = sum(1 for i in range(3, len(data)) if data[i] > data[i-3])
    return increased_count


def run():
    print(f"Day {DAY}")
    data = utils.load_input_as_lines(INPUT_PATH, as_int=True)
    print("Part 1", part_1(data))
    print("Part 1a", part_1a(data))

    print("Part 2 Test:", part_2([607, 618, 618, 617, 647, 716, 769, 792]))
    print("Part2", part_2(data))

    print("Part 2 a Test:", part_2a([607, 618, 618, 617, 647, 716, 769, 792]))
    print("Part 2 a", part_2a(data))

    print("Part 2 a Test:", part_2b([607, 618, 618, 617, 647, 716, 769, 792]))
    print("Part 2 a", part_2b(data))
