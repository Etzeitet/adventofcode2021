from statistics import mean, median
from pathlib import Path
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input7.txt"


def part_1(subs):
    median_fuel = median(subs)
    return sum(abs(x - median_fuel) for x in subs)


def part_2(subs):
    fuels = []
    to = int(mean(subs))

    for pos in subs:
        if pos > to:
            for j in range(1, pos - to + 1):
                fuels.append(j)

        elif pos < to:
            for j in range(1, to - pos + 1):
                fuels.append(j)

        else:
            fuels.append(0)

    return sum(fuels)





def run():
    print(f"Day {DAY}")
    data = utils.load_one_line_as_list(INPUT_PATH, as_int=True)

    sdata = sorted(data, reverse=True)
    for item in sdata:
        print(item)

    test_data = [16,1,2,0,4,2,7,1,2,14]

    print("Part 1 Test:", part_1(test_data))
    print("Part 1:", part_1(data))

    print("Part 2 Test:", part_2(test_data))
    print("Part 2:", part_2(data))
