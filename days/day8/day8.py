from pathlib import Path
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input8.txt"

test_data = """be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce
""".splitlines()


segments = [
    "abcef",
    "cf",
    "acdeg",
    "acdfg",
    "bcdf",
    "abdfg",
    "abdefg",
    "acf",
    "abcdefg",
    "abcdfg"
]

unique = [1, 4, 7, 8]


def get_signals(lines):
    signals = []

    for line in lines:
        input, output = line.split(" | ")
        inputs = input.strip().split()
        outputs = output.strip().split()

        signals.append([inputs, outputs])

    return signals


def part_1(signals):
    unique_count = 0

    for signal in signals:
        unique_count += sum(1 for x in signal[1] if len(x) in [2, 3, 4, 7])

    return unique_count


def part_2(signals):
    digit_total = 0

    for input, output in signals:
        power = 3

        # store digit by length and segments
        l = {len(seg): set(seg) for seg in input}

        for out in map(set, output):
            # segments length
            outlen = len(out)

            # length of intersection of digit 4 with out
            o4_mask = len(out & l[4])

            # length of intersection of digit 2 with out
            o2_mask = len(out & l[2])




            match outlen, o4_mask, o2_mask:
                # if the number of segments is unique, we know which digit it is
                # 2 segments for 1, 3 for 7, 4 for 4, and 7 for 8
                case 2,_,_:
                    digit_total += 1 * 10 ** power
                case 3,_,_:
                    digit_total += 7 * 10 ** power
                case 4,_,_:
                    digit_total += 4 * 10 ** power
                case 7,_,_:
                    digit_total += 8 * 10 ** power


                case 5,2,_:
                    digit_total += 2 * 10 ** power
                case 5,3,1:
                    digit_total += 5 * 10 ** power
                case 5,3,2:
                    digit_total += 3 * 10 ** power
                case 6,4,_:
                    digit_total += 9 * 10 ** power
                case 6,3,1:
                    digit_total += 6 * 10 ** power
                case 6,3,2:
                    digit_total += 0 * 10 ** power

            power -= 1

    return digit_total


def run():
    print(f"Day {DAY}")
    data = utils.load_input_as_lines(INPUT_PATH)

    print("Part 1 Test:", part_1(get_signals(test_data)))
    print("Part 1:", part_1(get_signals(data)))

    print("Part 2 Test:", part_2(get_signals(test_data)))
    print("Part 2:", part_2(get_signals(data)))
