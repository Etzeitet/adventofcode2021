import heapq
from dataclasses import dataclass
from pathlib import Path
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input15.txt"

test_data = """1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581
""".splitlines()


def get_cave(lines):
    cave = [list(map(int, line.strip())) for line in lines]
    height, width = len(cave), len(cave[0])

    return cave, height, width


def in_cave(y, x, height, width, cave_size):
    # since the cave can be tiled, make sure y, x coords is valid
    return 0 <= y < height * cave_size and 0 <= x < width * cave_size


def risk_level(cave, height, width, y, x):
    # risk level increase by one for each tiling, so adjust risk based on which
    # tile x, y point is located
    risk_level = cave[y % height][x % width] + x // height + y // width
    return risk_level % 9 or risk_level


def dijkstras(cave, height, width, cave_size=1):
    # starting node
    src = (0, 0)

    # destination node - adjust coordinates to adjust for tiling of caves
    dest = (height * cave_size - 1, width * cave_size - 1)

    # initialise priority queue (well, a list that will *become* our queue)
    heap = [(0,) + src]

    # populate visited with source node
    visited = {src}

    while heap:
        # get the least risk from queue
        risk, y, x = heapq.heappop(heap)

        # if we are at the destination, return the total risk
        if (y, x) == dest:
            return risk

        # for each connected neighbour (top, right, bottom, and left points)
        for ox, oy in [(0, -1), (1, 0), (0, 1), (-1, 0)]:
            dx = x + ox
            dy = y + oy

            # if node visited or outside of bounds of cave, continue
            if (dy, dx) in visited or not in_cave(dy, dx, height, width, cave_size):
                continue

            # current risk is this points risk, adjusted for tiling
            current_risk = risk_level(cave, height, width, dy, dx)

            # push this node and total risk onto queue
            heapq.heappush(heap, (risk + current_risk, dy, dx))

            # add to visited set
            visited.add((dy, dx))


def part_1(cave, height, width):
    return dijkstras(cave, height, width, cave_size=1)


def part_2(cave, height, width):
    return dijkstras(cave, height, width, cave_size=5)


def run():
    print(f"Day {DAY}")
    data = utils.load_input_as_lines(INPUT_PATH)

    print("Part 1 Test:", part_1(*get_cave(test_data)))
    print("Part 1:", part_1(*get_cave(data)))

    print("Part 2 Test:", part_2(*get_cave(test_data)))
    print("Part 2:", part_2(*get_cave(data)))
