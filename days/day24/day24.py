from pathlib import Path
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input24.txt"


class ALU:
    def __init__(self, program):
        self.pointer = 0
        self.program = program
        self.vars = {
            "w": 0,
            "x": 0,
            "y": 0,
            "z": 0
        }

        self.instructions = self.parse_program()

    def parse_program(self):
        instructions = []

        for line in self.program:
            parts = line.split()

            if len(parts) == 2:
                instruction, a = parts
                b = ""

            else:
                instruction, a, b = parts

            if b.replace("-", "").replace(".", "").isnumeric():
                b = int(b)

            instructions.append((instruction, a, b))

        return instructions

    def reset(self):
        for var in self.vars:
            self.vars[var] = 0

    def run(self, inputs):
        for instruction in self.instructions:
            match instruction:
                case "inp", a, b:
                    input = inputs.pop(0)
                    self.vars[a] = input

                case "add", a, b:
                    a_value = self.vars[a]
                    b_value = self.vars.get(b, b)

                    self.vars[a] = a_value + b_value

                case "mul", a, b:
                    a_value = self.vars[a]
                    b_value = self.vars.get(b, b)

                    self.vars[a] = a_value * b_value

                case "div", a, b:
                    a_value = self.vars[a]
                    b_value = self.vars.get(b, b)

                    self.vars[a] = a_value // b_value

                case "mod", a, b:
                    a_value = self.vars[a]
                    b_value = self.vars.get(b, b)

                    self.vars[a] = a_value % b_value

                case "eql", a, b:
                    a_value = self.vars[a]
                    b_value = self.vars.get(b, b)

                    self.vars[a] = int(a_value == b_value)


def part_1(program, min, max):
    alu = ALU(program)
    max_model_number = 0

    for model_number in range(min, max + 1):
        input = [int(d) for d in str(model_number)]

        if 0 in input:
            continue

        alu.run(input)
        # print(alu.vars)
        if alu.vars["z"] == 0 and model_number > max_model_number:
            max_model_number = model_number

        alu.reset()
        print((model_number / max) * 100)

    return max_model_number


def part_2():
    pass


def run():
    print(f"Day {DAY}")
    data = utils.load_input_as_lines(INPUT_PATH)

    test_programs = [
        (["inp w", "add w 23"], [10], {"w": 33}),
        (["inp x", "mul x -1"], [10], {"x": -10}),
        (["inp z", "inp x", "mul z 3", "eql z x"], [10, 30], {"z": 1}),
        ([
            "inp w",
            "add z w",
            "mod z 2",
            "div w 2",
            "add y w",
            "mod y 2",
            "div w 2",
            "add x w",
            "mod x 2",
            "div w 2",
            "mod w 2",
        ], [101], {"z": 1, "y": 0, "x": 1, "w": 0})
    ]

    for program, input, expected in test_programs:
        alu = ALU(program)
        alu.run(input)
        print(alu.vars)
        for var, value in expected.items():
            assert alu.vars[var] == value

    print("Part 1 Test:", part_1(data, 13579246899999, 13579246899999))
    print("Part 1:", part_1(data, 11111111111111, 99999999999999))
