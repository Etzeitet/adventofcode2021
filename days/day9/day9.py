from pathlib import Path
import days.util.utils as utils

DAY = Path(__file__).parts[-2][3:]
INPUT_PATH = Path(__file__).parent / "input9.txt"

test_data = """2199943210
3987894921
9856789892
8767896789
9899965678
""".splitlines()


def create_map(hmap):
    """
    Create grid of integers from list of strings:

    ["1234", "5678", ...] => [[1, 2, 3, 4], [5, 6, 7, 8], ...]
    """
    nmap = [[int(x) for x in row] for row in hmap]
    return nmap


def get_neighbours(hmap, x, y):
    """
    Return list of neighbours of x, y in hmap and their value as a 3-tuple:

    [(2, 3, 5), (4, 3, 7) ...]
    """
    max_x = len(hmap[0]) - 1
    max_y = len(hmap) - 1

    n = []
    for dx, dy in ((0,-1), (1,0), (0, 1), (-1, 0)):
        nx = x + dx
        ny = y + dy

        if 0 <= nx <= max_x and 0 <= ny <= max_y:
            n.append((nx, ny, hmap[ny][nx]))

    return n


def get_low_points(hmap):
    """
    Given the hmap, return a dictionary with all the low points (points that are lower
    than any neighbouring points).

    Keys are coordinates of the point, value is the value.
    """
    low_points = {}

    for y, row in enumerate(hmap):
        for x, col in enumerate(row):
            neighbours = get_neighbours(hmap, x, y)

            if all(col < h[-1] for h in neighbours):
                low_points[(x, y)] = col

    return low_points


def part_1(hmap):
    low_points = get_low_points(hmap)
    return sum(low_points.values()) + len(low_points)


def part_2(hmap):
    low_points = list(get_low_points(hmap).keys())
    basins = []

    # every low point is a basin
    for lx, ly in low_points:

        # add low point as first item in basin
        basin = {(lx, ly): hmap[ly][lx]}

        # load up low point's neighbouring values
        stack = get_neighbours(hmap, lx, ly)

        while stack:
            x, y, h = stack.pop(0)

            # value of 9 is a boundary between basins, so not included
            if h == 9:
                continue

            # we've already seen this point
            if (x, y) in basin:
                continue

            # add point to current basin
            basin[(x, y)] = h

            # get this point's neighbours
            stack.extend(get_neighbours(hmap, x, y))

        basins.append(basin)

    basins = sorted(basins, key=len)

    # for basin in basins:
    #     for k, v in basin.items():
    #         print(f"{v} ", end="")

    #     print()

    return len(basins[-3]) * len(basins[-2]) * len(basins[-1])


def run():
    print(f"Day {DAY}")
    data = utils.load_input_as_lines(INPUT_PATH)

    print("Part 1 Test:", part_1(create_map(test_data)))
    print("Part 1:", part_1(create_map(data)))

    print("Part 2 Test:", part_2(create_map(test_data)))
    print("Part 2:", part_2(create_map(data)))
