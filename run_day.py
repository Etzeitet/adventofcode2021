import sys
import importlib

day = f"day{sys.argv[1]}"

day_module = importlib.import_module(f"days.{day}.{day}")

day_module.run()
